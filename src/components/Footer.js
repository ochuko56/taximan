import React, { Component } from 'react'
import './Footer.css';

class Footer extends Component {
	render() {
		return (
			<footer className="footer">
		      <div className="container">
		        <span className="text-muted">&#169; Taximan {new Date().getFullYear()}.</span>
		        <span>Privacy</span>
		        <span>Terms</span>
		        <span className="social">
			        <i className="fab fa-facebook-f"></i>
			        <i className="fab fa-twitter"></i>
			        <i className="fab fa-instagram"></i>
		        </span>
		      </div>
		    </footer>
		);
	}
}

export default Footer;