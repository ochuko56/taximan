import React, { Component } from 'react'
import { Link } from 'react-router-dom';
import './Navbar.css';
import Taximan from '../images/taxi3.png'

class Navbar extends Component {
	render() {
		return (
			<nav className="navbar navbar-expand-lg navbar-dark bg-dark">
				<div className="container">
				  <Link className="navbar-brand" to="/"><img src={Taximan} alt="R-Icon" /><span>Taximan</span></Link>
				  <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
				    <span className="navbar-toggler-icon"></span>
				  </button>

				    <div className="collapse navbar-collapse" id="navbarSupportedContent">
					  <ul className="navbar-nav ml-auto">
					  	<li className="nav-item signup-link">
					      <Link to="/Signup" className="nav-link userX">Sign up</Link>
						</li>

						<li className="nav-item login-link">
					      <Link to="/Login" className="nav-link userX">Log in</Link>
						</li>

					    <li className="nav-item">
					      <Link className="nav-link" to="/Howitworks">How it works </Link>
					    </li>

						<li className="nav-item">
					      <Link className="nav-link" to="/Contact">Contact Us</Link>
						</li>

					  </ul>
					</div>
				</div>
			</nav>
		);
	}
}

export default Navbar;