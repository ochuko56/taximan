import React, { Component } from 'react'
import { Container, Row, Col, Media, Form, FormGroup, Input, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import './Signup.css';

class Signup extends Component {
	render() {
		return (
			<div>
				<div className="body-bg">
					<Container>
						<Row>
							<Col sm="7" className="bg-text">
								
								<h1>Signup with Taximan</h1>
							</Col>
							<Col sm="5">
								<div className="form-panel">
									<Form>
										<Link to="/Login" className="btn btn-dark btn-block">Already have an Account?</Link>
										<h6>Select user type</h6>
										<FormGroup>
										  <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
										    <option selected>Driver</option>
										    <option value="2">Rider</option>
										    <option value="3">Admin</option>
										  </select>
										</FormGroup>
										<Row>
											<Col>
											    <FormGroup>
											        <Input type="name" name="firstname" id="firstName" placeholder="First name" />
											    </FormGroup>
										    </Col>
										    <Col>
											    <FormGroup>
											        <Input type="name" name="lastname" id="lastName" placeholder="Last name" />
											    </FormGroup>
										    </Col>
									    </Row>
									    <FormGroup>
										  <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
										    <option selected>Gender</option>
										    <option value="1">Male</option>
										    <option value="2">Female</option>
										  </select>
										</FormGroup>
									    <FormGroup>
									        <Input type="email" name="email" id="userEmail" placeholder="Email" />
									    </FormGroup>
									    <FormGroup>
									      <Input type="tel" name="phonenum" id="phonenum" placeholder="Phone"/>
									    </FormGroup>
									    <FormGroup>
									      <Input type="password" name="pwd" id="password" placeholder="Password"/>
									    </FormGroup>
									    <p>By submitting, you agree that all information provided is correct.</p>
									    <Button className="btn btn-block" color="primary">Submit</Button>
									</Form>
								</div>
							</Col>
						</Row>
					</Container>
				</div>
				
			</div>
		);
	}
}

export default Signup;