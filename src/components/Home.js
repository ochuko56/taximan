import React, { Component } from 'react'
import { Container, Row, Col, Media, Form, FormGroup, Input, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import './Home.css';
import Rideshare from '../images/ride-share1.jpg';
import PeoplePost from '../images/unsplash1.jpg';

class Home extends Component {
	render() {
		return (
			<div>
				<div>
					<Media src={Rideshare} />
					<Container>
						<Row>
							<Col sm="5" className="home-welcome">
								<h3>Going somewhere?</h3>
								<p>We've got you covered</p>
								<h1>Commute easy with Taximan</h1>
							</Col>
							<Col sm="7" className="signup-user">
								<Link to="/Signup" className="btn btn-primary">Become a Driver</Link>
								<Link to="/Signup" className="btn btn-primary">Sign up and ride</Link>
							</Col>
						</Row>
					</Container>
					<div className="bg-1">
						<Row className="section-2">
							<Col lg="4">
								<h3>Pool Ride</h3>
								<p>Join with a fellow on the road or choose a classic ride.</p>
								<Link to="/">Learn more </Link>
							</Col>
							<Col sm>
								<i className="fas fa-3x fa-car"></i>
							</Col>
							<Col lg="7">
								<Media src={PeoplePost} width="100%"/>
							</Col>
						</Row>
					</div>
				</div>

				<div className="bg-2">
					<Container>
						<Row className="section-3">
							<Col lg="12" className="">
								<h3>How it Works</h3>
								<p>place content here</p>
							</Col>
						</Row>
					</Container>
				</div>

				<div className="bg-3">
					<Container>
						<Row className="section-4">
							<Col lg="12" className="">
								<h3>User Stories/Feedback</h3>
								<p>place content here</p>
							</Col>
						</Row>
					</Container>
				</div>

				<div className="more-about">
			        	<Container>
			        		<Row>
					          <Col sm="6">
					          	<h3>About</h3>
								<p>
									Taximan si na chara gracia taxi company al services un revo ai driver si rider. Booke la driver la rider da dei rest of the world. Founded in 2018.
								</p>
					          </Col>
			        			
					          <Col sm="6">
					          	<h3>Contact us</h3>
									<Form>
								        <FormGroup>
								          <Input type="email" name="email" id="userEmail" placeholder="Email" />
								          
								        </FormGroup>
								        <FormGroup>
								                  <Input type="tel" name="name" id="fullName" placeholder="Name" />
								        </FormGroup>
								        <FormGroup>
								          <Input type="textarea" name="text" id="textArea" placeholder="Message"/>
								        </FormGroup>
								        <Button className="btn btn-outline-dark">Send</Button>
      								</Form>
					          </Col>
						    </Row>
					    </Container>
			        </div>

			</div>
		);
	}
}

export default Home;