import React, { Component } from 'react'
import { Container, Row, Col, Media, Form, FormGroup, Input, Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import './Login.css';

class Login extends Component {
	render() {
		return (
			<div>
				<div className="body-bg1">
					<Container>
						<Row>
							<Col>
							<h5>Taximan User</h5>
							<div className="form-panel1">
								<Form>
									<FormGroup>
										<label for="email">Email</label>
										<Input type="email" name="email" id="userEmail" placeholder="Enter email" />
									</FormGroup>
									<FormGroup>
										<label for="password">Password</label>
										<Input type="password" name="pwd" id="password" placeholder="Enter password"/>
									</FormGroup>
									<p>Don't have an account? <Link to="/Signup">Sign up</Link></p>
									<Button className="btn btn-block" color="primary">Login</Button>
								</Form>
							</div>
							</Col>
						</Row>
					</Container>
				</div>
			</div>
		);
	}
}

export default Login;