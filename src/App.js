import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';
import './App.css';

import Navbar from './components/Navbar';
import Footer from './components/Footer';
import Home from './components/Home';
import Howitworks from './components/Howitworks';
import Contact from './components/Contact';
import Signup from './components/Signup';
import Login from './components/Login';

class App extends Component {
  render() {
    return (
      <Router>
        <div>
          <Navbar />
          <Route exact path="/" component={Home} />
          <Route path="/Howitworks" component={Howitworks} />
          <Route path="/Contact" component={Contact} />
          <Route path="/Signup" component={Signup} />
          <Route path="/Login" component={Login} />
          <Footer />
        </div>
      </Router>
    );
  }
}

export default App;
